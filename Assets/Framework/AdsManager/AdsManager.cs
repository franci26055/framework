﻿using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.Advertisements; //Library that include ads

public class AdsManager : MonoBehaviour, IUnityAdsListener
{
    #region Variables & Properties
    public const string myGameIdIOS = "3832114";
    public const string myGameIdAndroid = "3832115";
    
    //Ads type. Same name of the placementID in the Dashboard
    public const string myVideoPlacement = "video";
    public const string myRewardedVideoPlacement = "rewardedVideo";
    public const string myBannerPlacement = "banner";

    public bool adStarted;
    public bool adCompleted;

    public bool testMode = true;

    [SerializeField] private TextMeshProUGUI txt_Error; 
    #endregion

    #region MonoBehaviour

    private void Start()
    {
        InitializePlatform();

        Advertisement.AddListener(this);

        StartCoroutine(ShowBanner());
    }

    #endregion

    #region Methods
    /// <summary>
    /// Initialize the ads based on the platform that the application is currently running.
    /// </summary>
    private void InitializePlatform()
    {
#if UNITY_IOS
        Advertisement.Initialize(myGameIdIOS, testMode);
#endif

#if UNITY_ANDROID
        Advertisement.Initialize(myGameIdAndroid, testMode);
#endif

#if UNITY_EDITOR
        Advertisement.Initialize(myGameIdAndroid, testMode);
#endif
    }

    /// <summary>
    /// Show a video ad at full screen.
    /// </summary>
    /// <param name="isRewarded">The type of the video. True if rewarded, false skippable.</param>
    public void ShowAd(bool isRewarded)
    {
        string adType = isRewarded ? myRewardedVideoPlacement : myVideoPlacement;

        if (Advertisement.isInitialized && !adStarted && Advertisement.IsReady(adType))
        {
            Advertisement.Show(adType);
            adStarted = true;
        }
        else
        {
            txt_Error.SetText("Ad not ready");
        }
    }

    private IEnumerator ShowBanner()
    {
        while (!Advertisement.isInitialized)
            yield return null;

        while(!Advertisement.IsReady(myBannerPlacement))
            yield return null;

        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Banner.Show(myBannerPlacement);

        txt_Error.SetText(Advertisement.Banner.isLoaded ? "Banner displayed" : "Banner not ready");
       
        yield return null;
    }

    //Interface implementation
    public void OnUnityAdsReady(string placementId)
    {
        print("Ready " + placementId);
    }

    public void OnUnityAdsDidError(string message)
    {
        txt_Error.SetText("Error: " + message);
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        print("Started " + placementId);
        adCompleted = false;
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        adCompleted = showResult == ShowResult.Finished;
        adStarted = false;
        print("Finish " + placementId);
    }
    #endregion
}