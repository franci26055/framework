using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : Singleton<ObjectPooler>
{
    private Dictionary<string, Queue<GameObject>> poolDictionary;
    [SerializeField] private List<Pool> pools;
    [SerializeField] private bool shouldUseTheExpandMethod;

    protected override void Awake()
    {
        base.Awake();
        InitPool();
    }


    /// <summary>
    /// 
    /// </summary>
    void InitPool()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        Queue<GameObject> objectPool;

        foreach (Pool pool in pools)
        {
            objectPool = new Queue<GameObject>();
            GameObject parent = new GameObject(pool.tag + "parent");
            parent.transform.SetParent(this.transform);

            for (int i = 0; i < pool.amountToPool; i++)
            {
                GameObject obj = Instantiate(pool.objectToPool, parent.transform);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            poolDictionary.Add(pool.tag, objectPool);
        }
    }


    #region SpawnObject
    public GameObject GetPooledObject(in string _tag, in Vector3 position, in Quaternion rotation)
    {
        GameObject pooledObj = GetPooledObject(in _tag, position);
        pooledObj.transform.rotation = rotation;
        return pooledObj;
    }
    public GameObject GetPooledObject(in string _tag, in Vector3 position)
    {
        GameObject pooledObj = GetPooledObject(in _tag);
        pooledObj.transform.position = position;
        return pooledObj;
    }
    public GameObject GetPooledObject(in string  _tag)
    {
        if (!poolDictionary.ContainsKey(_tag))
        {
            Debug.LogWarning("There is no object in the pool called " + "'" + _tag + "'");
            return null;
        }

        GameObject objectToSpawn = poolDictionary[_tag].Dequeue();
        #region Expanding the pool

        print(_tag + "  :" +poolDictionary[_tag].Count);

        if (shouldUseTheExpandMethod)
        {
            if (poolDictionary[_tag].Count == 0)
            {
                foreach (Pool pool in pools)
                {
                    if (pool.tag.Equals(_tag) && pool.shouldExpand)
                    {

                        objectToSpawn = pool.objectToPool;
                        Instantiate(pool.objectToPool);
                        poolDictionary[_tag].Enqueue(pool.objectToPool);

                        return objectToSpawn;

                    }
                }
            }
        }

        #endregion

        objectToSpawn.SetActive(true);

        return objectToSpawn;
    }

    
   
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_tag">Object tag</param>
    /// <returns>Object to spawn</returns>

    #endregion
}


[System.Serializable]
public class Pool
{
    public string tag;
    public GameObject objectToPool;
    public int amountToPool;
    public bool shouldExpand;
}
