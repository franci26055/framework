using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class InputManager : MonoBehaviour
{
    #region Zoom
    float touchDist = 0;
    float lastDist = 0;
    #endregion

    #region DoubleTap
    float MaxTimeWait = 1;
    float VariancePosition = 1;
    #endregion

    #region Swipe
    [SerializeField] float swipeRange, tapRange;
    private Vector2 startPosition, CurrentPosition, endPosition;
    private bool stopTou = false;
    [SerializeField] public bool isSwipe = false;
    #endregion

    private float count = 0;
    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch firstTouch = Input.GetTouch(0);
            TouchPhases(firstTouch);
            DoubleTap(firstTouch.tapCount);
            if (Input.touchCount == 2) { Zoom(); }
            if (isSwipe)
            {
                Swipe(firstTouch);
            }

        }
    }

    private void TouchPhases(in Touch touch)
    {
        switch (touch.phase)
        {
            case TouchPhase.Began:
                print("tap");
                break;
            case TouchPhase.Moved:
                print("Move");
                break;
            case TouchPhase.Stationary:
                count += 0.001f;
                break;
            case TouchPhase.Ended:
                break;
            case TouchPhase.Canceled:
                print("canceled");
                break;
            default:
                break;
        }
    }

    private void Zoom()
    {
        Touch touch1 = Input.GetTouch(0);
        Touch touch2 = Input.GetTouch(1);

        if (touch1.phase == TouchPhase.Began && touch2.phase == TouchPhase.Began)
        {
            lastDist = Vector2.Distance(touch1.position, touch2.position);
        }

        if (touch1.phase == TouchPhase.Moved && touch2.phase == TouchPhase.Moved)
        {
            float newDist = Vector2.Distance(touch1.position, touch2.position);
            touchDist = lastDist - newDist;
            lastDist = newDist;

            // Your Code Here
            Camera.main.fieldOfView += touchDist * 0.1f;
        }
    }

    private void DoubleTap(int touch)
    {
        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            float DeltaTime = Input.GetTouch(0).deltaTime;
            float DeltaPositionLenght = Input.GetTouch(0).deltaPosition.magnitude;

            if (DeltaTime > 0 && DeltaTime < MaxTimeWait && DeltaPositionLenght < VariancePosition && touch == 2) { print("doubleTap"); }
        }
    }

    private void Swipe(Touch touch)
    {
        if (touch.phase == TouchPhase.Began)
        {
            startPosition = touch.position;
        }

        if (touch.phase == TouchPhase.Moved)
        {
            CurrentPosition = touch.position;
            Vector2 Distance = CurrentPosition - startPosition;

            if (!stopTou)
            {
                if (Distance.x < -swipeRange)
                {
                    print("left");
                    stopTou = true;
                }
                else if (Distance.x > swipeRange)
                {
                    print("right");
                    stopTou = true;
                }
                else if (Distance.y < -swipeRange)
                {
                    print("down");
                    stopTou = true;
                }
                else if (Distance.y > swipeRange)
                {
                   print("up");
                    stopTou = true;
                }

            }
        }

        if (touch.phase == TouchPhase.Ended)
        {
            stopTou = false;
            endPosition = touch.position;
            Vector2 distance = endPosition - startPosition;

            if (Mathf.Abs(distance.x) < tapRange && Mathf.Abs(distance.y) < tapRange)
            {
                print("tap");
            }
        }
    }
}
