using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void Save()
    {

        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/SaveItem.Save";
        FileStream stream = new FileStream(path, FileMode.Create);

        Object data = new Object();

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static Object LoadSave()
    {
        string path = Application.persistentDataPath + "/SaveItem.Save";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            Object data = formatter.Deserialize(stream) as Object;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("SaveFileNoteFound in" + path);
            return null;
        }
    }
}

[System.Serializable]
public class Object : MonoBehaviour
{
    float score;
}


